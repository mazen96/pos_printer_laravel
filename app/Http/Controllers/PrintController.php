<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//require __DIR__ . '/../../autoload.php';
//require 'vendor/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\CapabilityProfiles\EposTepCapabilityProfile;
use Mike42\Escpos\PrintBuffers\ImagePrintBuffer;
use Mike42\Escpos\Escpos;



//use Mike42\Escpos\ImagickEscposImage;
//use Mike42\Escpos\GdEscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


/*
 * Drop Ar-php into the folder listed below:
 */
//require_once(dirname(__FILE__) . "/../../I18N/Arabic.php");
use Johntaa\Arabic\I18N_Arabic;
//$fontPath = dirname(__FILE__) . "/../../I18N/Arabic/Examples/GD/ae_AlHor.ttf";

//require_once(dirname(__FILE__) . "/../../Escpos.php");


class PrintController extends Controller
{
    public function getHome()
    {
        return view('home');
    }

    function print() {

        
        /*$textUtf8 = "صِف خَلقَ خَودِ كَمِثلِ الشَمسِ إِذ بَزَغَت — يَحظى الضَجيعُ بِها نَجلاءَ مِعطارِ";
        $maxChars = 50;
        $fontSize = 28;
        //$fontPath =  dirname(__FILE__) ."/../../../vendor/johntaa/ar-php/src//I18N/Arabic/Examples/GD/ae_AlHor.ttf";
        $fontPath = 'C:\xampp\htdocs\POS_Printer_Demo\vendor\johntaa\ar-php\src\Arabic\Arabic\Examples\GD\ae_AlHor.ttf';
        
        mb_internal_encoding("UTF-8");
        $Arabic = new I18N_Arabic('Glyphs');
        $textLtr = $Arabic -> utf8Glyphs($textUtf8, $maxChars);
        $textLine = explode("\n", $textLtr);
        
        $buffer = new ImagePrintBuffer();
        $buffer -> setFont($fontPath);
        $buffer -> setFontSize($fontSize);
        $profile = EposTepCapabilityProfile::getInstance();*/
        //$profile = CapabilityProfile::load("TEP-200M");
        //$connector = new FilePrintConnector("php://output");
                // = new WindowsPrintConnector("LPT2");
                // Windows LPT2 was used in the bug tracker
        //$printer = new Printer($connector, $profile);
        //$printer -> setPrintBuffer($buffer);
        //$printer -> setJustification(Printer::JUSTIFY_RIGHT);
        
        
        /* Fill in your own connector here */
        try
        {
            $connector = null;
            //$connector = new WindowsPrintConnector('XP-80C');
            //$printer = new Printer($connector, $profile);
            //$printer -> setPrintBuffer($buffer);
            //$printer -> setJustification(Printer::JUSTIFY_RIGHT);
            //$printer->text("نعم نحن رجال !!\n");

            mb_internal_encoding("UTF-8");
            $Arabic = new I18N_Arabic('Glyphs');
            $text = "صِف خَلقَ خَودِ كَمِثلِ الشَمسِ إِذ بَزَغَت — يَحظى الضَجيعُ بِها نَجلاءَ مِعطارِ";
            $text = $Arabic -> utf8Glyphs($text);
            /*
            * Set up and use the printer
            */
            $buffer = new ImagePrintBuffer();
            $profile = EposTepCapabilityProfile::getInstance();
            $connector = new WindowsPrintConnector('XP-80C');
                    // = WindowsPrintConnector("LPT2");
                    // Windows LPT2 was used in the bug tracker
            $printer = new Printer($connector, $profile);
            $printer -> setPrintBuffer($buffer);
            $printer -> setJustification(Printer::JUSTIFY_RIGHT);
            $printer -> text($text . "\n");
            //$printer -> close();

            /*
            // Information for the receipt 
            $items = array(
                new item("Example item #1", "4.00"),
                new item("Another thing", "3.50"),
                new item("Something else", "1.00"),
                new item("A final item", "4.45"),
            );
            $subtotal = new item('Subtotal', '12.95');
            $tax = new item('A local tax', '1.30');
            $total = new item('Total', '14.25', true);
            // Date is kept the same for testing
            // $date = date('l jS \of F Y h:i:s A');
            $date = "Monday 6th of April 2015 02:56:25 PM";

            // Start the printer 
            // \escpos-php.png
            $logo = EscposImage::load(public_path('pdfFiles\cr7.jpg'), false);
            $printer = new Printer($connector, $profile);

            $printer -> setPrintBuffer($buffer);
        

            // Print top logo 
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->bitImage($logo);

            // Name of shop 
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text("ExampleMart Ltd.\n");
            $printer->selectPrintMode();
            $printer->text("Shop No. 42.\n");
            $printer->feed();

            // Title of receipt 
            $printer->setEmphasis(true);
            //$printer->text("SALES INVOICE\n");
            $printer->text("فاتورة\n");
            $printer->setEmphasis(false);

            // Items 
            $printer -> setJustification(Printer::JUSTIFY_RIGHT);
            $printer->setEmphasis(true);
            $printer->text(new item('', '$'));
            $printer->setEmphasis(false);
            foreach ($items as $item) {
                $printer->text($item);
            }
            $printer->setEmphasis(true);
            $printer->text($subtotal);
            $printer->setEmphasis(false);
            $printer->feed();

            // Tax and total
            $printer->text($tax);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text($total);
            $printer->selectPrintMode();

            // Footer
            $printer->feed(2);
            $printer -> setJustification(Printer::JUSTIFY_RIGHT);
            $printer->text("Thank you for shopping at ExampleMart\n");
            $printer->text("For trading hours, please visit example.com\n");
            $printer->feed(2);
            $printer->text($date . "\n");

            */
            // Cut the receipt and open the cash drawer
            $printer->cut();
            //$printer->pulse();

            $printer->close();
        }
        catch (Exception $e)
        {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
        

    }
}

/* A wrapper to do organise item names & prices into columns */
class item
{
    private $name;
    private $price;
    private $dollarSign;

    public function __construct($name = '', $price = '', $dollarSign = false)
    {
        $this -> name = $name;
        $this -> price = $price;
        $this -> dollarSign = $dollarSign;
    }
    
    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 38;
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> name, $leftCols) ;
        
        $sign = ($this -> dollarSign ? '$ ' : '');
        $right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}

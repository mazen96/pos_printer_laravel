<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use PDF;
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\ImagickEscposImage;
use Mike42\Escpos\GdEscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;



class PdfGenerateController extends Controller
{
    public function pdfview(Request $request)
    {
        $users = DB::table("users")->get();
        view()->share('users',$users);

        /*if($request->has('download')){
        	// Set extra option
        	PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        	// pass view file
            $pdf = PDF::loadView('pdfview');
            // download pdf
            return $pdf->download('pdfview.pdf');
        }*/

        //if($request->has('download'))
        //{
            // Set extra option
        	PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            // pass view file
            $storepath = public_path().'/pdfFiles/bill.pdf';
            $pdf = PDF::loadView('pdfview')->save($storepath);;
            // download pdf
            //return $pdf->download('pdfview.pdf');

            try {
                // Enter the share name for your USB printer here
                //$connector = null;
                $connector = new WindowsPrintConnector('XP-80C');
                //$connector = new WindowsPrintConnector("Receipt Printer");
            
                /* Print a "Hello world" receipt" */
                $printer = new Printer($connector);
                for($i=0 ; $i < 1 ; $i=$i+1)
                {
                    $pages = ImagickEscposImage::loadPdf($storepath);
                    //$pages = EscposImage::load($storepath);
                    //$pages = GdEscposImage::loadImageData($storepath);
                    foreach ($pages as $page) {
                        $printer -> bitImage($page);
                    }
                    $printer -> cut();

                }
                
                    //$printer -> text("Snack Max");

                    //$printer -> cut();
                    
                    /* Close printer */
                    $printer -> close();
                } catch (Exception $e) {
                    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
                }
        //}

        //return view('pdfview');
        return view('home');
    }
}
